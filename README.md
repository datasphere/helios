[_English presentation_](#presentation)  
[_Présentation en Français_](#présentation)  


# Presentation

![Helios banner](https://sharedocs.huma-num.fr/wl/?id=FhFlfKYC3XbPrFNEwTiRcyaIq7vaGa6Q&fmode=download)



## What is this?

Helios is a minimal webserver giving any software temporary access to files stored locally on your machine.
In particular, softwares running in your browser.



## Why should I use it?

If you are in the following situation ...

- you use a software that runs in your browser, ...
- you want this software to have access to your local files, ...
- you want your local files to remain separated from your software, ...
- you don't have access to a remote file storage when you use your software, ...

... Then this minimal server allows you to give your software (temporary) access to your local files.

**In other words:**

You work offline, either because you are working on an excavation site or in an archive basement without any reliable wireless Internet access.
You have all the files you need in an external hard drive.
And you want these files to be viewable from inside an application.



## How can I use it?

Just  [download Helios](https://gitlab.com/datasphere.science/helios/-/raw/master/run-http-server.command?inline=false), and put it in the folder you want to give another software access to.

Then run Helios (for example by double clicking on it) : a dialog opens, and your files are now available via [http://localhost:8000/](http://127.0.0.1:8000/).
You can visit this URL using your web browser to validate it.

When you're done working the other software, you can just close the dialog.
Your files remain stored on your machine, but they won't be accessible any more (except maybe if they are stored in your [browser's cache](https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache)).

**An example, please?**

If your corpus is inside the folder `my_storage/my_project/my_corpus`, and you want the file  `my_storage/my_project/my_corpus/my_images/my_image.png` to be displayed by your software, move the script `run-http-server.command` inside the folder `my_storage/my_project/my_corpus`, run it, a dialog opens and your file will be available at the following url: `http://localhost:8000/my_images/my_image.png`.
In your software, write this URL wherever a file location is expected, and it will be displayed as long as the dialog remains open.
Of course, this only works on your computer.

**It doesn't work?**

Helios is a Python program, so make sure [Python is installed](https://docs.python.org/3/using/index.html).
Maybe you'll need some additional libraries, too (`tkinter` and `http.server`, or their equivalents in Python2.x, although these librairies are generally present -that's why I chose them).

Maybe the port `8000` is already used in your machine.  
In that case, you can:

1. either just close the offending application ;
2. or [edit this line](https://gitlab.com/datasphere.science/helios/-/blob/master/run-http-server.command#L22) so Helios uses another port.  
   Of course, you'll then have to replace `8000` by your chosen port in all the instructions above.

I've made this software work with no modification on different versions of Linux and MacOS.
It _should_ work on Windows too, but I won't bother testing as Windows is overpriced and just sucks overall.



## Is it any good?

Well, for the specific use case I described, it does the job.  
This isn't a full fledged server though, it only has minimal security (there isn't even any HTTPS support), so only use this with software you trust, and in an "offline" environment.



## Are there any related repositories?

Yes !  
Other softwares are available both :
* [on the "general purpose" repository GitLab.com](https://gitlab.com/datasphere.science/), as well as
* [on the repository hosted by the french research infrastucture Huma-Num](https://gitlab.huma-num.fr/datasphere/)



## And who are you?

[Maison Interuniversitaire des Sciences de l'Homme - Alsace (MISHA — UAR 3227, ArcHiMedE — UMR 7044)](https://www.misha.fr/)





<br><br><br><br><br>





# Présentation

![Bannière d'Helios](https://sharedocs.huma-num.fr/wl/?id=FhFlfKYC3XbPrFNEwTiRcyaIq7vaGa6Q&fmode=download)



## Quel est ce projet ?

Helios est un serveur web minimal qui vous permet de donner temporairement accès à vos fichiers locaux à n'importe quel logiciel.
En particulier, à un logiciel tournant dans votre navigateur.



## Pourquoi devrais-je l'utiliser ?

Si vous êtes dans la situation suivante ...

- vous utilisez un logiciel tournant dans votre navigateur Internet, ...
- vous voulez que ce logiciel ait accès à vos fichiers locaux, ...
- vous voulez conserver vos fichiers séparés de ce logiciel, ...
- vous n'avez pas accès à un stockage institutionnel de fichiers distant tel que [Sharedocs](https://sharedocs.huma-num.fr/), ou à un entrepôt institutionnel pérenne comme [Nakala](https://www.nakala.fr/) ou [data.gouv](https://www.data.gouv.fr/), ...

... alors, ce projet vous permet de donner au logiciel un accès (temporaire) à vos fichiers locaux.

**Cas pratique :**

Vous avez besoin de travailler sans accès Internet, soit parce que vous travaillez sur un chantier de fouilles ou dans des archives à la cave sans avoir de connexion Wi-Fi/3G/4G/5G fiable.
Cependant, vous avez vos fichiers (_eg._ votre corpus scientifique de référence) sur votre disque dur ou un de ses support externe.
Et vous voulez rendre ces fichiers visibles par une application tierce.



## Comment puis-je l'utiliser ?

[Téléchargez simplement Helios](https://gitlab.com/datasphere.science/helios/-/raw/master/run-http-server.command?inline=false), et mettez-le dans le fichier racine du corpus auquel vous voulez que votre logiciel ait accès.
Ensuite, exécutez Helios (par exemple en double-cliquant dessus) : une boîte de dialogue s'ouvre, et vos fichiers sont maintenant disponibles à l'adresse [http://localhost:8000/](http://127.0.0.1:8000/).
Vous pouvez vous en convaincre en visitant cette adresse via votre navigateur Internet.

Lorsque vous avez terminé votre travail avec votre logiciel, vous pouvez simplement fermer la boîte de dialogue.
Vos fichiers ne seront alors plus accessiblesen qu'ils resteront évidemment stockés sur votre machine.  
Si vous voyez encore vos fichiers depuis votre navigateur après avoir fermé la boîte de dialogue, c'est parce qu'ils sont encore dans le [cache de votre navigateur](https://fr.wikipedia.org/wiki/Aide:Purge_du_cache_du_navigateur).

**Un exemple ?**

Si votre corpus se trouve dans le dossier `mon_disque/mon_projet/mon_corpus`, et que vous voulez que soit visible le fichier  `mon_disque/mon_projet/mon_corpus/mes_images/mon_image.png`, posez le script `run-http-server.command` dans le dossier `mon_disque/mon_projet/mon_corpus`, lancez-le, et votre fichier sera disponible à l'adresse `http://localhost:8000/mes_images/mon_image.png`.
Entrez cette adresse comme valeur d'une métadonnée, et votre image sera affichée aussi longtemps que la boîte de dialogue d'Helios est ouverte.
Évidemment, cela ne fonctionnera que sur votre ordinateur.

**Ça ne fonctionne pas ?**

Helios s'agit d'un programme en langage Python, donc si ça ne fonctionne pas directement, assurez-vous que [Python soit installé sur votre machine](https://docs.python.org/fr/3/using/index.html).
Il est possible que vous ayez besoin d'installer une ou deux dépendances additionnelles (`tkinter` et `http.server`, ou leurs équivalents si vous utilisez Python2.x, même si j'ai choisi ces librairies justement car elles sont souvent inclues dans une installation standard de Python).

Il est aussi possible que le port `8000` soit déjà utilisé sur votre machine.  
Dans ce cas, vous avez deux solutions :

1. fermez l'application qui utilise le port `8000` ; 
2. [éditez cette ligne](https://gitlab.com/datasphere.science/helios/-/blob/master/run-http-server.command#L22) du fichier que vous avez téléchargé avec un nouveau port.  
   Évidemment, cela signifie qu'il faut remplacer `8000` par le port que vous avez choisi dans les instructions ci-dessus.

J'ai validé le fonctionnement de ce programme avec différentes versions de Linux et MacOS.
Ça _devrait_ fonctionner aussi sur Windows, mais je ne le testerai pas moi-même, Windows étant un OS cher, de mauvaise qualité et liberticide.



## Est-ce que c'est bien ?

Pour le cas d'usage décrit plus haut, ça fait le travail.  
Ce projet n'est pas un serveur web professionnel cependant ; il n'offre notamment que des fonctionnalités de sécurité très minimales (il n'y a même aucun support du protocole HTTPS), donc ne l'utilisez que pour communiquer avec des logiciels auxquels vous faites confiance, et dans un environnement réseau maîtrisé (ou absent).



## Y a t'il d'autres projets liés à celui-ci ?

Oui !  
Les différents logicels de ce projet sont disponibles à la fois :
* [sur la forge « grand public » GitLab.com](https://gitlab.com/datasphere.science/), ainsi que
* [sur la forge institutionnelle de l'infrastructure de recherche Huma-Num](https://gitlab.huma-num.fr/datasphere/)



## Et qui êtes-vous ?

[Maison Interuniversitaire des Sciences de l'Homme - Alsace (MISHA — UAR 3227, ArcHiMedE — UMR 7044)](https://www.misha.fr/)
