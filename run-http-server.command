#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:  # Python 3.x
    from http.server import HTTPServer, SimpleHTTPRequestHandler
    import tkinter as tk
    from tkinter import ttk
except ImportError:  # Python 2.x
    from SimpleHTTPServer import BaseHTTPServer
    HTTPServer = BaseHTTPServer.HTTPServer
    from SimpleHTTPServer import SimpleHTTPRequestHandler
    import Tkinter as tk
    import ttk
import threading
import os
# set working directory to where this file is stored
working_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(working_dir)

# run basic HTTP server -do NOT use this in production !
# this server runs in another thread and is killed by server.shutdown()
PORT = 8000
server = HTTPServer(('localhost', PORT), SimpleHTTPRequestHandler)
thread = threading.Thread(target = server.serve_forever)
thread.daemon = True
thread.start()

# create a tkinter frame with an exit callback
root = tk.Tk()
setattr(root, 'keep_running', True)  # main loop exit flag
def on_exit():  # this callback allows to exit main loop (see below)
    root.keep_running = False
panel = ttk.Frame(root, padding=10)
panel.grid()
ttk.Label(panel, text="Le dossier ci-dessous:").grid(column=0, row=0)
ttk.Label(panel, text="%s\n" % working_dir, font='bold').grid(column=0, row=1)
ttk.Label(panel, text="... est accessible à l'URL:").grid(column=0, row=2)
ttk.Label(panel, text="http://localhost:%s/\n" % server.server_port, font='bold').grid(column=0, row=3)
ttk.Button(panel, text="Quitter", command=on_exit).grid(column=0, row=4)
root.title("Serveur local")
root.protocol('WM_DELETE_WINDOW', on_exit)

# main loop
while root.keep_running:
    root.update_idletasks()
    root.update()

# graceful exit
root.destroy()
server.shutdown()
